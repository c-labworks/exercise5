﻿using System;
using System.Collections.Generic;

namespace Componovschic_212
{
    public class Folder : Directory
    {
        private List<Directory> m_children;
        private Directory parent = null;


        public Folder(Directory parent)
        {
            this.parent = parent;
            m_children = new List<Directory>();
            parent?.Add(this);
        }

        public override void GetDirName()
        {
            var level = "";
            var p = parent;
            while (p != null)
            {
                level += "    ";
                p = p.GetParent();
            }

            level += ToString();
            Console.WriteLine(level);
            foreach (Directory dir in m_children)
            {
                dir.GetDirName();
            }
        }

        public override void Add(Directory dir)
        {
            m_children.Add(dir);
        }

        public override void Remove(Directory dir)
        {
            m_children.Remove(dir);
        }

        public override Directory GetChild(int index)
        {
            return m_children[index];
        }
        
        public override Directory GetParent()
        {
            return parent;
        }
    }
}
