﻿namespace Componovschic_212
{
    public abstract class Directory
    {
        public abstract void GetDirName();

        public virtual void Add(Directory dir)
        {            
        }

        public virtual void Remove(Directory dir)
        {
        }

        public virtual Directory GetChild(int index)
        {
            return null;
        }

        public abstract Directory GetParent();
    }
}
