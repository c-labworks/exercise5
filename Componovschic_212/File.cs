﻿using System;

namespace Componovschic_212
{
    public class File : Directory
    {
        //private string m_sName = string.Empty;

        private Directory parent = null;

        public File(Directory parent)
        {
            this.parent = parent;
            parent.Add(this);
        }

        public override Directory GetParent()
        {
            return parent;
        }

        public override void GetDirName()
        {
            var level = "";
            var p = parent;
            while (p != null)
            {
                level += "    ";
                p = p.GetParent();
            }

            level += ToString();
            Console.WriteLine(level);
        }
    }
}
